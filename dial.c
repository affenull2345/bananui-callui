#include <string.h>
#include <stdio.h>
#include <dbus/dbus.h>
#include "modem-ofono.h"

int dial(DBusConnection *connection, const char *modempath,
	const char *number)
{
	DBusMessage *callmsg;
	DBusMessageIter args;
	DBusError error;
	const char *hide_callerid = "";

	printf("Dialing %s with modem %s...\n", number, modempath);

	dbus_error_init(&error);
	callmsg = dbus_message_new_method_call("org.ofono", modempath,
		"org.ofono.VoiceCallManager", "Dial");
	if(!callmsg){
		fprintf(stderr, "Failed to create call to Dial\n");
		return -1;
	}

	dbus_message_iter_init_append(callmsg, &args);
	dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &number);
	dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &hide_callerid);

	dbus_connection_send_with_reply_and_block(connection, callmsg, 3000,
		&error);
	if(dbus_error_is_set(&error)){
		fprintf(stderr, "%s: %s\n", modempath, error.message);
		dbus_error_free(&error);
		return -1;
	}
	return 0;
}

/* Return value: -1=error, 0=keep going, 1=dialing */
int checkInterfaceAndDial(DBusConnection *connection, const char *modempath,
	DBusMessageIter *interface, const char *number)
{
	char *interfacename;
	while(1){
		if(dbus_message_iter_get_arg_type(interface) !=
			DBUS_TYPE_STRING)
		{
			return 0;
		}
		dbus_message_iter_get_basic(interface, &interfacename);
		if(0 == strcmp(interfacename, "org.ofono.VoiceCallManager")){
			return dial(connection, modempath, number) < 0 ? -1 : 1;
		}
		if(!dbus_message_iter_next(interface)) break;
	}
	return 0;
}

/* Return value: -1=error, 0=keep going, 1=dialing */
int checkAndDial(DBusConnection *connection, const char *modempath,
	DBusMessageIter *dict, const char *number)
{
	DBusMessageIter dent, keyval, val, interface;
	int res;

	dbus_message_iter_recurse(dict, &dent);
	while(1){
		char *propname;
		if(dbus_message_iter_get_arg_type(&dent) !=
			DBUS_TYPE_DICT_ENTRY)
		{
			fprintf(stderr, "%s: No properties or invalid dict\n",
				modempath);
			return -1;
		}
		dbus_message_iter_recurse(&dent, &keyval);
		if(dbus_message_iter_get_arg_type(&keyval) !=
			DBUS_TYPE_STRING)
		{
			fprintf(stderr, "%s: Property name not string\n",
				modempath);
			return -1;
		}
		dbus_message_iter_get_basic(&keyval, &propname);
		if(!dbus_message_iter_next(&keyval)){
			fprintf(stderr, "%s: %s: No value\n", modempath,
				propname);
			return -1;
		}
		if(dbus_message_iter_get_arg_type(&keyval) !=
			DBUS_TYPE_VARIANT)
		{
			fprintf(stderr, "%s: %s: Value is not a variant\n",
				modempath, propname);
			return -1;
		}
		dbus_message_iter_recurse(&keyval, &val);
		if(0 == strcmp(propname, "Interfaces")){
			if(dbus_message_iter_get_arg_type(&val) !=
				DBUS_TYPE_ARRAY)
			{
				fprintf(stderr,
					"%s: Interfaces property not array\n",
					modempath);
				return -1;
			}
			dbus_message_iter_recurse(&val, &interface);
			res = checkInterfaceAndDial(connection, modempath,
				&interface, number);
			if(res < 0) return -1;
			else if(res) return 1;
		}
		if(!dbus_message_iter_next(&dent)) break;
	}
	return 0;
}

int main(int argc, const char **argv)
{
	DBusError error;
	DBusConnection *connection;
	DBusMessage *callmsg, *replymsg;
	DBusMessageIter reply, replyarray, modem;
	int res = 0;

	if(argc < 2){
		fprintf(stderr, "Usage: %s phonenumber\n", argv[0]);
		return 1;
	}
	dbus_error_init(&error);
	connection = dbus_bus_get(DBUS_BUS_SYSTEM, &error);
	if(dbus_error_is_set(&error)){
		fprintf(stderr, "Failed to connect to system bus: %s\n",
			error.message);
		dbus_error_free(&error);
		return 2;
	}
	callmsg = dbus_message_new_method_call("org.ofono", "/",
		"org.ofono.Manager", "GetModems");
	if(!callmsg){
		fprintf(stderr, "Couldn't create call to GetModems\n");
		return 2;
	}
	replymsg = dbus_connection_send_with_reply_and_block(
		connection, callmsg, 3000, &error);
	if(dbus_error_is_set(&error)){
		fprintf(stderr, "Failed to get modem list: %s\n",
			error.message);
		dbus_error_free(&error);
		return 2;
	}
	if(!replymsg || !dbus_message_iter_init(replymsg, &reply)){
		fprintf(stderr, "No reply to GetModems!\n");
	}
	if(dbus_message_iter_get_arg_type(&reply) !=
		DBUS_TYPE_ARRAY)
	{
		fprintf(stderr, "Reply from GetModems not an array!\n");
	}
	dbus_message_iter_recurse(&reply, &replyarray);

	while(1){
		char *path;
		if(dbus_message_iter_get_arg_type(&replyarray) !=
			DBUS_TYPE_STRUCT)
		{
			fprintf(stderr, "No modems or invalid reply\n");
			return 2;
		}
		dbus_message_iter_recurse(&replyarray, &modem);
		if(dbus_message_iter_get_arg_type(&modem) !=
			DBUS_TYPE_OBJECT_PATH)
		{
			fprintf(stderr, "First field of modem struct is not an object path\n");
			return 2;
		}
		dbus_message_iter_get_basic(&modem, &path);
		if(!dbus_message_iter_next(&modem)){
			fprintf(stderr, "Modem has no properties!\n");
			if(!dbus_message_iter_next(&replyarray)) break;
			continue;
		}
		if(dbus_message_iter_get_arg_type(&modem) !=
			DBUS_TYPE_ARRAY ||
			dbus_message_iter_get_element_type(&modem) !=
			DBUS_TYPE_DICT_ENTRY)
		{
			fprintf(stderr,
				"Modem properties field is not a dict\n");
			return 2;
		}

		res = checkAndDial(connection, path, &modem, argv[1]);
		if(res > 0) return 0;

		if(!dbus_message_iter_next(&replyarray)) break;
	}
	if(res < 0) return 2;
	return 0;
}
