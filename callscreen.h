/*
 * BananUI user interface for calls - Call screen
 * Copyright (C) 2021 Affe Null <affenull2345@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _CALLSCREEN_H_
#define _CALLSCREEN_H_

#include <time.h>
#include <bananui/bananui.h>
#include "calls.h"

struct callscreen_data {
	bWindow *wnd;		/* The call screen window */
	bWidget icon;		/* An icon with call-start or call-end */
	bWidget statelbl;	/* A label widget showing the call state */
	bWidget timelbl;	/* A label widget showing the call time */
	bWidget numlbl;		/* A label widget showing the phone number */
	bWidget namelbl;	/* A label widget showing the caller's name */
	enum call_state state;
	struct daemon_data *dd;
};
void updateCallscreen(struct callscreen_data *csdata, enum call_state callstate,
	const char *time, const char *number, const char *name);
int callscreenCallback(bEventType type, bEventData *evdata);
int showCallscreen(struct daemon_data *dd, struct callscreen_data *csdata,
	const char *number, const char *name, enum call_state callstate);
void destroyCallscreen(struct callscreen_data *csdata);
int callscreen_getRingTimeout(struct callscreen_data *csdata,
	struct timespec *tv);
void callscreen_handleRingTimeout(struct callscreen_data *csdata);
int callscreen_handleResponse(struct callscreen_data *csdata, int fd);
void callscreen_getConnectionFds(struct callscreen_data *csdata,
	int *fds, size_t *numfds);

#endif
