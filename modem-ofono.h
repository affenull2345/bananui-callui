/*
 * BananUI user interface for calls - oFono backend
 * Copyright (C) 2021 Affe Null <affenull2345@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _MODEM_OFONO_H_
#define _MODEM_OFONO_H_

#include <dbus/dbus.h>
#include "calls.h"

void modem_ofono_initializeDBus(DBusConnection *connection, DBusError *error,
	struct daemon_data *dd, void **modem_data);
void modem_ofono_startup(struct daemon_data *dd, void **modem_data);
void modem_ofono_handleMessage(DBusMessage *msg, struct daemon_data *dd,
	void **modem_data);
void modem_ofono_acceptIncomingCall(struct daemon_data *dd, void **modem_data);
void modem_ofono_rejectIncomingCall(struct daemon_data *dd, void **modem_data);
void modem_ofono_hangupCall(struct daemon_data *dd, void **modem_data);

#endif
