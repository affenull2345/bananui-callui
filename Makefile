PKG_CONFIG = pkg-config
CFLAGS = -Wall -g $(shell $(PKG_CONFIG) --cflags dbus-1)
LDFLAGS = $(shell $(PKG_CONFIG) --libs dbus-1)
OBJECTS = daemon.o modem-ofono.o callscreen.o
DIALOBJECTS = dial.o
OUTPUTS = bananui-callui bananui-dial

all: $(OUTPUTS)

bananui-callui: $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) -lbananui $(LDFLAGS) $(CFLAGS)

bananui-dial: $(DIALOBJECTS)
	$(CC) -o $@ $(DIALOBJECTS) $(LDFLAGS) $(CFLAGS)

%.o: %.c
	$(CC) -o $@ -c $*.c $(CFLAGS)

clean:
	rm -f $(OBJECTS) $(OUTPUTS)

install:
	install $(OUTPUTS) $(DESTDIR)/usr/bin
