/*
 * BananUI user interface for calls - Call screen
 * Copyright (C) 2021 Affe Null <affenull2345@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <unistd.h>
#include <time.h>
#include <linux/input-event-codes.h>
#include "callscreen.h"
#include "calls.h"

void updateCallscreen(struct callscreen_data *csdata, enum call_state callstate,
	const char *time, const char *number, const char *name)
{
	csdata->state = callstate;
	csdata->statelbl = bCreateLabel(csdata->wnd,
		CALLSTATE_TO_STRING(callstate), TA_CENTER, csdata->statelbl);
	csdata->icon = bCreateIcon(csdata->wnd, CALLSTATE_TO_ICON(callstate),
		TA_CENTER, 128, 128, csdata->icon);
	if(time){
		csdata->timelbl = bCreateLabel(csdata->wnd, time, TA_CENTER,
			csdata->timelbl);
	}
	if(number){
		csdata->numlbl = bCreateLabel(csdata->wnd, number, TA_CENTER,
			csdata->numlbl);
	}
	if(name){
		csdata->namelbl = bCreateLabel(csdata->wnd, name, TA_CENTER,
			csdata->namelbl);
	}
	switch(callstate){
		case CALL_STATE_INCOMING:
			bWakeup(csdata->wnd);
			bVibrate(csdata->wnd, 400);
			bSetSoftkeys(csdata->wnd, "Accept", "", "Reject");
			break;
		case CALL_STATE_FAILED:
			bSetSoftkeys(csdata->wnd, "", "CLOSE", "");
			break;
		default:
			bSetSoftkeys(csdata->wnd, "", "", "");
	}
	bRefreshWnd(csdata->wnd, 0);
}


int callscreenCallback(bEventType type, bEventData *evdata)
{
	struct callscreen_data *csdata;
	csdata = bGetWindowData(evdata->window);
	if(type == BEV_EXIT){
		if(csdata->state == CALL_STATE_FAILED) return 0;
		hangupCall(csdata->dd);
	}
	else if(type == BEV_KEYDOWN && evdata->val == KEY_BACK &&
		csdata->state == CALL_STATE_INCOMING)
	{
		rejectIncomingCall(csdata->dd);
	}
	else if(type == BEV_KEYDOWN &&
		(evdata->val == KEY_MENU || evdata->val == KEY_SEND) &&
		csdata->state == CALL_STATE_INCOMING)
	{
		acceptIncomingCall(csdata->dd);
	}
	else if(type == BEV_KEYDOWN && evdata->val == KEY_ENTER &&
		csdata->state == CALL_STATE_FAILED)
	{
		return 0;
	}
	return -1;
}

int showCallscreen(struct daemon_data *dd, struct callscreen_data *csdata,
	const char *number, const char *name, enum call_state callstate)
{
	csdata->dd = dd;
	csdata->wnd = bCreateWnd();
	if(!csdata->wnd) return -1;
	bSetWindowTitle(wnd, "@call-start@Call");
	csdata->icon = bCreateIcon(csdata->wnd, CALLSTATE_TO_ICON(callstate),
		TA_CENTER, 128, 128, NO_WIDGET);
	bCreateLabel(csdata->wnd, "\n", TA_CENTER, NO_WIDGET);
	csdata->numlbl = bCreateLabel(csdata->wnd, number, TA_CENTER,
		NO_WIDGET);
	csdata->namelbl = bCreateLabel(csdata->wnd, (name && name[0]) ?
		name : "\n", TA_CENTER, NO_WIDGET);
	csdata->state = callstate;
	csdata->statelbl = bCreateLabel(csdata->wnd,
		CALLSTATE_TO_STRING(callstate), TA_CENTER, NO_WIDGET);
	csdata->timelbl = bCreateLabel(csdata->wnd, "\n", TA_CENTER, NO_WIDGET);
	switch(callstate){
		case CALL_STATE_INCOMING:
			bWakeup(csdata->wnd);
			bSetSoftkeys(csdata->wnd, "Accept", "", "Reject");
			bVibrate(csdata->wnd, 400);
			break;
		case CALL_STATE_FAILED:
			bSetSoftkeys(csdata->wnd, "", "CLOSE", "");
			break;
		default:
			bSetSoftkeys(csdata->wnd, "", "", "");
	}
	bSetWindowData(csdata->wnd, csdata);
	bSetEventCallback(csdata->wnd, callscreenCallback);
	bRefreshWnd(csdata->wnd, 0);
	return 0;
}

void destroyCallscreen(struct callscreen_data *csdata)
{
	bCloseWnd(csdata->wnd);
}

int callscreen_getRingTimeout(struct callscreen_data *csdata,
	struct timespec *timeout)
{
	if(csdata->state != CALL_STATE_INCOMING) return 0;
	timeout->tv_sec = 3;
	timeout->tv_nsec = 0;
	return 1;
}

void callscreen_handleRingTimeout(struct callscreen_data *csdata)
{
	if(csdata->state != CALL_STATE_INCOMING) return;
	bVibrate(csdata->wnd, 400);
}

int callscreen_handleResponse(struct callscreen_data *csdata, int fd)
{
	return bHandleResponse(csdata->wnd, fd);
}

void callscreen_getConnectionFds(struct callscreen_data *csdata,
	int *fds, size_t *numfds)
{
	bGetConnectionFds(csdata->wnd, fds, numfds);
}
